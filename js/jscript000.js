function Results($scope)
{
    $scope.max          = 3;
    $scope.simple       = false;
    $scope.popularShow  = false;
    $scope.loading      = false;
    $scope.showPopup    = false;
    $scope.showPopup1   = false;
    $scope.last         = 30;
    $scope.users        = [];
    $scope.userDefined  = false;
    $scope.message      = "";
    $scope.myUser       = [];
    $scope.filter       = $.jStorage.get('filter');
    $scope.user         = [{avatar: '', name: '', surname: '', curves: '', speed:'', time: ''}];
    $scope.basicSwitch  = function(){
        if($scope.filter == 'popular')
            $scope.popularShow = true;
    }
    $scope.next = function(){
        var place = parseInt($scope.user.place);
        place++;
        if(place==$scope.max)return;
        $scope.getPlace(place);
    }
    $scope.last = function(){
        var place = parseInt($scope.user.place);
        place--;
        if(place==0)return;
        $scope.getPlace(place);
    }
    $scope.getPlace = function(place)
    {
        console.log('place '+place);
        $scope.loading = true;
        $scope.showPopup1 = true;
        $.ajax({
            url: '?r=users/getplace',
            type: "POST",
            data: {place: place, filter: $scope.filter},
            success: function(data){
                $scope.loading = false;
                var request = JSON.parse(data);
                if(request.status==1){
                    return;
                }else{
                    $scope.max = request.max;
                    $scope.user = request.data;
                }
                $scope.$apply();
            }
        });
    }
    $scope.checkSimple  = function(){
        $.ajax({
            url: 'index.php?r=users/checksimple',
            type: 'post',
            success: function(data)
            {
                var result = JSON.parse(data);
                if(result.status==0){
                    $scope.simple = false;
                    $scope.$apply();
                }else{
                    $scope.simple = true;
                    $scope.$apply();
                }
            }
        });
    }
    $scope.vote = function(id)
    {
        $scope.voice_mess = "Обработка запроса";
        console.log('give vote '+ id);
        $.ajax({
            url: 'index.php?r=users/vote',
            type: "post",
            data: {id: id, type: 'simple'},
            success: function(data)
            {
                 var request = JSON.parse(data);
                 setTimeout(function(){
                 if(request.status==1)
                     {
                         $scope.user.likes = request.likes;
                         $scope.voice_mess = "Успешно"
                         $scope.$apply();
                         $scope.checkSimple();
                     }
                     else{
                         $scope.voice_mess = "";
                         $scope.$apply();
                     }
                },50);
            }});
    }
    $scope.voteVK = function(id)
    {
        $scope.voice_mess = "Обработка запроса";
        console.log('give vote '+ id);
        $.ajax({
            url: 'index.php?r=users/vote',
            type: "post",
            data: {id: id, type: 'vk'},
            success: function(data)
            {
                 var request = JSON.parse(data);
                 setTimeout(function(){
                 if(request.status==1)
                     {
                         $scope.user.likes = request.likes;
                         $scope.voice_mess = "Успешно";
                         $scope.$apply();
                         $scope.checkSimple();
                     }
                     else{
                         $scope.voice_mess = "";
                         $scope.$apply();
                     }
                },50);
            }});
    }
    $scope.voteFB = function(id)
    {
        $scope.voice_mess = "Обработка запроса";
        console.log('give vote '+ id);
        $.ajax({
            url: 'index.php?r=users/vote',
            type: "post",
            data: {id: id, type: 'fb'},
            success: function(data)
            {
                 var request = JSON.parse(data);
                 setTimeout(function(){
                 if(request.status==1)
                     {                        
                         $scope.voice_mess = "Успешно";
                         $scope.user.likes = request.likes;
                         $scope.$apply();
                         $scope.checkSimple();
                     }
                     else{
                         $scope.voice_mess = "";
                         $scope.$apply();
                     }
                },50);
            }});
    }
    $scope.getCurvesString = function(num)
    {
        var last = num%10;
        var pre_last = num%100;
        if(last==0||(pre_last>4&&pre_last<19)||(last>4&&last<10)){
            return "виражей";
        }else if(last>1&&last<5){
            return "виража";
        }else
            return "вираж";
    }
    $scope.basicSwitch();
    $scope.showResult = function()
    {
        console.log('show result');
        $scope.showPopup = !$scope.showPopup;
        console.log($scope.showPopup);
    }
    $scope.findUser = function()
    {
        if($scope.user.requisites=='')return;
        $scope.message = "Идет обработка запроса...";
        $.ajax({
            url: '?r=users/define',
            type: "POST",
            data: {requisites: $scope.user.requisites},
            success: function(data){
                var request = JSON.parse(data);
                if(request.status==0){
                    $scope.userDefined = true;
                    $scope.myUser = request.data;
                    $scope.$apply();
                }else{
                    $scope.message = request.message;
                    $scope.$apply();
                }
            }
        });
    }
    $scope.getUser = function(id)
    {
        $scope.loading = true;
        $scope.showPopup1 = true;
        $.ajax({
            url: '?r=users/getuser',
            type: "POST",
            data: {id: id, filter: $scope.filter},
            success: function(data){
                console.log(data);
                var user = JSON.parse(data);
                if(user.id=='')return;
                $scope.user = user;
                console.log(user.id);
                $scope.loading = false;
                $scope.$apply();
            }
        });
    }
    $scope.switchPopular = function()
    {
            $scope.popularShow = true;
            $.jStorage.set('filter', 'popular');
            $scope.filter = $.jStorage.get('filter');
            console.log($.jStorage.get('filter'));
    }
    $scope.seeAll = function()
    {
        if($scope.filter==''){
            $.jStorage.set('filter', 'fast');
        }
        document.location = 'index.php?r=site/page&view=results';
    }
    $scope.switchFast = function()
    {
        $scope.popularShow = false;
        $.jStorage.set('filter', 'fast');
        $scope.filter = $.jStorage.get('filter');
        console.log($.jStorage.get('filter'));
    }
    $scope.closePopup = function()
    {
        $scope.showPopup = false;
    }
    $scope.closePopup1 = function()
    {
        $scope.showPopup1 = false;
    }
    $scope.getWidth = function(){
        $scope.width = window.innerWidth;
    }
    $scope.showMyPosition = function()
    {
        $.jStorage.set('test', 'ok');
        console.log($.jStorage.get('test'));
        console.log('Вывод позиции');
    }
    $scope.showMoreResults = function()
    {
        $.ajax({
            url: "?r=users/more",
            type: "POST",
            data: {last: $scope.last,
                    filter: $scope.filter},
            success: function(data){
                var new_users = JSON.parse(data);
                var time = 100;
                angular.forEach(new_users, function(user){
                    setTimeout(function(){
                        $scope.users.push(user);
                        $scope.$apply();
                    }, time);
                    time+=100;
                });
                if(new_users.length<20)
                    {
                        $('.refresh').hide();
                    }
                $scope.last += 20;
            }
        });
    }
    $scope.checkSimple();
}
function SinglePopup($scope)
{
    $scope.showSPopup = true;
    $scope.closeSPopup = function()
    {
        $scope.showSPopup = false;
    }
}