<?php

class SmsController extends Controller
{
	public function actionIndex()
	{
		echo "SMS controller";
	}

        private function exec($url, $fields="", $method="POST")
        {
            $ch = curl_init();  
            curl_setopt($ch, CURLOPT_URL,$url); // set url to post to  
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable  
            curl_setopt($ch, CURLOPT_TIMEOUT, 20); // times out after 20s  
            curl_setopt($ch, CURLOPT_POST, 1); // set POST method  
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$fields"); // add POST fields  
            $result = curl_exec($ch); // run the whole process  
            curl_close($ch);  
            return $result;
        }

        public function actionSend()
	{

	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}