<?php

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'more', 'choise', 'define', 
                                    'fill', 'create', 'getFirst', 'getSecond', 'getThird', 'getSeven',
                                    'getUser', 'vote', 'send', 'test', 'checksimple', 'cron', 'getPlace'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        public function actionTest()
        {
            echo 'ok';
        }
        public function actionCron()
        {
            $users = new Users();
            $all = $users->findAllBySql("SELECT * FROM `users` WHERE `checked`=0;");
            var_dump($all);
            foreach ($all as $user)
            {
                $new_users = new Users();
                $cur_user = $new_users->find("`id`=$user_id");
                
                            //$this->Send($post[$user->code], $post[$user->phone]);
                            
                            
                $cur_user->checked = 1;
                $cur_user->save();
                $check_user = $users->find("`id`='$user->id'");
            }
        }
        public function actionCheckSimple()
        {
            $ip         = $_SERVER['REMOTE_ADDR'];
            $likes      = new Likes();
            $current    = $likes->find("`ip`='$ip'");
            if(empty($current))
                echo json_encode ($result['status']=0);
            else
                echo json_encode ($result['status']=1);
        }                
        public function actionGetPlace()
        {
            if(preg_match('/^[0-9]+$/', $_POST['place'])){
                $request['status'] = 1;
                $users  = new Users();
                $place  = $_POST['place'];
                $all = $users->findAllBySql("SELECT * FROM `users`");
                $i = 0;
                foreach ($all as $one)
                {
                    $i++;
                    $request['max']=$i+1;
                }
                if($place>$i||$place==0)
                {
                    $request['status'] = 1;
                    echo json_encode($request);
                    return;                    
                }
                if(isset($_POST['filter']))
                    $filter = trim(stripslashes (htmlspecialchars ($_POST['filter'])));
                if($place>=0){
                    $user   = $users->getUserData($place, $filter);
                    $request['status']  = 0;
                    $request['data']    = $user;
                }
            echo json_encode($request);}
        }
        public function actionGetFirst()
        {
            if(isset($_POST['filter']))$filter = trim(htmlspecialchars(stripcslashes($_POST['filter'])));
            $users = new Users();
            return $users->getUserData(1, $filter);
        }
        public function actionGetSecond()
        {
            if(isset($_POST['filter']))$filter = trim(htmlspecialchars(stripcslashes($_POST['filter'])));
            $users = new Users();
            return $users->getUserData(2, $filter);
        }
        public function actionGetThird()
        {
            if(isset($_POST['filter']))$filter = trim(htmlspecialchars(stripcslashes($_POST['filter'])));
            $users = new Users();
            return $users->getUserData(3, $filter);
        }
        public function actionGetSeven()
        {
            if(isset($_POST['filter']))$filter = trim(htmlspecialchars(stripcslashes($_POST['filter'])));
            $users = new Users();
            return $users->getGroup(4, 7, $filter);
        }
        public function actionVote()
        {
            $type = trim(stripslashes(htmlspecialchars($_POST['type'])));
            if(preg_match("/^[0-9]+$/", $_POST['id']))
            {
                $ip_c               = $_SERVER['REMOTE_ADDR'];
                $id                 = $_POST['id'];
                $likes              = new Likes();
                $current            = $likes->find("`ip`='$ip_c'");
                if(empty($current))
                {
                    $likes->ip = $ip_c;
                    $likes->$type = 1;
                    $type_time = $type."_time";
                    $likes->$type_time = date("H:i:s d-m-Y");
                    $likes->save();
                    $users = new Users();
                    $user = $users->find("`id`=$id");
                    $likes = $user->likes;
                    $likes++;
                    $user->likes = $likes;
                    $user->save();
                    $result['status']=1;
                    $users = new Users();
                    $user = $users->find("`id`=$id");
                    $likes = $user->likes;
                    $result['likes']=$likes;
                }
                else
                {
                    if($current->$type==0){
                        $type_time = $type."_time";
                        $current->$type = 1;
                        $current->$type_time = date("H:i:s d-m-Y");
                        $current->save();
                        $users = new Users();
                        $user = $users->find("`id`=$id");
                        $l = $user->likes;
                        $l++;
                        $user->likes = $l;
                        $user->save();
                        $result['status']=1;
                        $users = new Users();
                        $user = $users->find("`id`=$id");
                        $likes = $user->likes;
                        $result['likes']=$likes;
                    }else{
                        $result['status']=0;
                    }
                }
                echo json_encode($result);
            }
        }        
        public function actionGetuser()
        {
            if(preg_match('/^[0-9]+$/', $_POST['id'])){
                $id     = $_POST['id'];
                $filter = '';
                if(isset($_POST['filter']))
                    $filter = trim(stripslashes (htmlspecialchars ($_POST['filter'])));
                $users  = new Users();
                $user   = $users->getUserById($id, $filter);
                echo json_encode($user);
            }
        }
        public function actionFill()
        {
            for($i=1;$i<50;$i++)
            {
                $users          = new Users();
                $users->name    = "User$i User_$i";
                $add            = "";
                if($i<10)
                    $add = 0;
                $time_last      = "$add$i";
                $users->time    = "00:00:$time_last";
                $users->curves  = "1$i";
                $users->speed   = 100;
                $users->email   = "email$i@adi.das";
                $users->phone   = "$i$i$i$i";
                $number         = $i%10;
                $users->avatar  = "Avatar$number.jpg";
                $users->likes   = rand(0, 100);
                $users->save();
            }
        }        
        public function actionDefine()
        {
            //request 0 - status, 1 - message, 2 - data
            $requisites = "";
            if(isset($_POST['requisites']))$requisites = trim(stripslashes(htmlspecialchars($_POST['requisites'])));
            if(!empty($requisites)){
                $users = new Users();
                $user = $users->find("`email`='$requisites'");
                if(!empty($user->name)){
                    $request['status']  = 0;
                    $user_r['name']     = $user->name;
                    $user_r['curves']   = $user->curves;
                    $user_r['speed']    = $user->speed;
                    $user_r['time']     = $user->time;
                    $user_r['place']    = $users->getPlace($user->id, 'fast');
                    $user_r['avatar']   = $user->avatar;
                    $user_r['id']       = $user->id;
                    $user_r['likes']    = $user->likes;
                    $request['data']    = $user_r;
                    echo json_encode($request);
                }else{
                    $user = $users->find("`phone`='$requisites'");
                    if(!empty($user->name)){
                        $request['status']  = 0;
                        $user_r['name']     = $user->name;
                        $user_r['curves']   = $user->curves;
                        $user_r['speed']    = $user->speed;
                        $user_r['time']     = $user->time;
                        $user_r['place']    = $users->getPlace($user->id, 'fast');
                        $user_r['avatar']   = $user->avatar;
                        $user_r['id']       = $user->id;
                        $user_r['likes']    = $user->likes;
                        $request['data']    = $user_r;
                        echo json_encode($request);
                    }else{
                        $request['status'] = 1;
                        $request['message'] = "Данные не найдены.";
                        echo json_encode($request);                        
                    }
                }
            }
        }        
        public function actionMore()
        {
            $first = $_POST['last'];
            if(isset($_POST['filter']))$filter=trim(htmlspecialchars (stripslashes ($_POST['filter'])));
            if($filter=='fast')
                $sql = "SELECT * FROM `users` ORDER BY time ASC, id ASC LIMIT $first, 20;";
            else
                $sql = "SELECT * FROM `users` ORDER BY likes DESC, id ASC LIMIT $first, 20;";
            $model = new Users();
            $more = $model->findAllBySql($sql);
            $request = array();
            $place = $first+1;
            foreach($more as $current)
            {
                $user['place'] = $place;
                $user['name'] = $current->name;
                $fixed_str                = preg_replace('/[\s]{2,}/', ' ', $current->name);
                    $fixed_str                = trim($fixed_str);
                    $name_array               = explode(" ", $fixed_str);
                    $user['name1']            = $name_array[0];
                 if(isset($name_array[1]))
                    $user['name2']            = $name_array[1];
                $user['curves'] = $current->curves;
                $user['speed'] = $current->speed;
                $user['time'] = $current->time;
                $user['id'] = $current->id;
                $user['likes'] = $current->likes;
                array_push($request, $user);
                $place++;
            }
            echo json_encode($request);
        }

        /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            //var_dump($_POST);exit;
		$model=new Users;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
                        $post = $_POST['Users'];
                        $post['avatar'] = $_FILES['avatar']['name'];
                        $code = file_get_contents('protected/files/code.php');
                        $code++;
                        file_put_contents('protected/files/code.php', $code);
                        $post['code'] = $code;
			$model->attributes=$post;
                        $destination = 'images/Avatars/'.$_FILES['avatar']['name'];
                        if(isset($_FILES['avatar']['type'])){
                            $type = $_FILES['avatar']['type'];
                            if(move_uploaded_file($_FILES['avatar']['tmp_name'], $destination)){
                                echo "Изображение сохранено";
                            }
                        }
                        if($model->save())
                        {
                            
                            $email = $post['email'];
                            $u_model = Users::model()->find("`email`='$email'");
                            $u_model->time = $post['time'];
                            $u_model->save();
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
