<div ng-class="{'overlay': showPopup1}" ng-click='showPopup1=false'></div>
<div ng-init="checkSimple()" ng-class="{'hidden': !showPopup1, 'popup': showPopup1}">
    <div class="popup_body">
        <div ng-class="{'hidden': loading}">
            <div class='popup_line'>
                <div style="display: block; width: 170px; height: 170px;">
                    <img ng-src="images/Avatars/{{user.avatar}}" width="170" height="170"/>
                </div>
                <span class='popup_text1'>{{user.name1}}<br>{{user.name2}}</span>
                <div class='popup_place'>{{user.place}}</div>
                <img class='popup_time_image' class='popup_image' src='../images/popup_time.png' width='50' height='50'/>
                <span class='popup_time'>{{user.time}}</span>
                <span class='popup_text2'>Ваше время</span>
            </div>
            <div class='popup_line'>
                <img class='curves_image' src='../images/popup_curves.jpg' width='30' height='30'/>
                <span class='curves_count'>{{user.curves}}</span>
                <span class='curves_text'>{{getCurvesString(user.curves)}}</span>
                <img class='speed_image' src='../images/popup_speed.jpg' width='43' height='37'/>
                <span class='speed_count'>{{user.speed}} км/ч</span>
                <span class='speed_text'>средняя скорость</span>
            </div>            
            <div class='popup_footer'>
                <img class='popup_to_tell' src='../images/voice.png' width='207' height='45'/>
                <?php
                    $summary = 'Делитесь ссылкой на свои результаты в Facebook, и повышайте свой рейтинг';
                    $url = "http://".$_SERVER['SERVER_NAME']."/index.php?id={{user.id}}";
                    $image_url = 'http://'.$_SERVER['SERVER_NAME'].'/images/Avatars/{{user.avatar}}';
                ?>
                <a ng-class="{'hidden': !simple}" ng-click="voteFB(user.id)" class='footer_fasebook' href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo  $url ; ?>&p[summary]=<?php echo $summary ?>&p[images][0]=<?php echo $image_url ?>" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" title="Поделиться ссылкой на Фейсбук" target="_parent"><img src='../images/popup_facebook.png' width='41' height='38'/></a>
                <?php
                    $description = 'Делитесь ссылкой на свои результаты "в контакте", и повышайте свой рейтинг';
                    $url = "http://".$_SERVER['SERVER_NAME']."/index.php?id={{user.id}}";
                    $image_url = 'http://'.$_SERVER['SERVER_NAME'].'/images/Avatars/{{user.avatar}}';
                ?>
                <a ng-class="{'hidden': !simple}" ng-click="voteVK(user.id)" class='footer_vk' href="http://vkontakte.ru/share.php?url=<?php echo $url; ?>&description=<?php echo $description; ?>&image=<?php echo $image_url; ?>&noparse=true" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" title="Сохранить в Вконтакте" target="_parent"><img src='../images/popup_vk.jpg' width='41' height='37'/></a>
                <a ng-class="{'hidden': simple}" class="footer_like_image" ng-click="vote(user.id)" class='footer_like' href=""><img src='../images/icons/like.png' width='33' height='27'/></a>
                <span class="footer_like_text">{{user.likes}}</span>
                <span style='position: absolute; right: 20px; top: 65px;'>{{voice_mess}}</span>
            </div>
        </div>
    </div>
    <div ng-click="closePopup1()" class="popup_close"><img src="../images/popup_close.png" width="50" height="50"/></div>
    <div ng-click="last()" class="last_button"><img href="images/icons/last.png" width="148" height="148"/></div>
    <div ng-click="next()" class="next_button"><img href="images/icons/next.png" width="148" height="148"/></div>
</div>