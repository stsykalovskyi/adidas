<div ng-class="{'overlay': showPopup}" ng-click='showPopup=false'></div>
<div ng-init="checkSimple()" ng-class="{'hidden': !showPopup, 'popup': showPopup}">
    <div class="popup_body">
        <div ng-show="!userDefined" class='padding_20'>
            <div style='margin-top: 35px; text-align: center; font-size: 22px !important;'>Для того, чтобы узнать результат нужно отправить свой телефон или Email</div><br><br>
            <form ng-submit="findUser()" name="findForm">
                <div style='display: block; width: 408px; height: 55px; margin: 0 auto;'>
                    <input style='text-align: center; background: #F0F0F0;' class="popup_input" ng-model="user.requisites" name="email" type="test" placeholder='Email или телефон'/><br>
                </div>
                    <span class="error">{{message}}</span>
                    <br><div class='popup_button' ng-click='findUser()'></div>
            </form>
        </div>
        <div ng-show="userDefined">
            <div class='popup_line'>
                <img ng-src="{{myUser.avatar}}" width="170" height="170"/>
                <span class='popup_text1'>Мой результат заезда</span>
                <div class='popup_place'>{{myUser.place}}</div>
                <img class='popup_time_image' class='popup_image' src='../images/popup_time.png' width='50' height='50'/>
                <span class='popup_time'>{{myUser.time}}</span>
                <span class='popup_text2'>Ваше время</span>
            </div>
            <div class='popup_line'>
                <img class='curves_image' src='../images/popup_curves.jpg' width='30' height='30'/>
                <span class='curves_count'>{{myUser.curves}}</span>
                <span class='curves_text'>{{getCurvesString(user.curves)}}</span>
                <img class='speed_image' src='../images/popup_speed.jpg' width='43' height='37'/>
                <span class='speed_count'>{{myUser.speed}} км/ч</span>
                <span class='speed_text'>средняя скорость</span>
            </div>            
            <div class='popup_footer'>
                <img class='popup_to_tell' src='../images/popup_to_tell.png' width='207' height='45'/>
                <?php
                    $summary = 'Делитесь ссылкой на свои результаты в Facebook, и повышайте свой рейтинг';
                    $url = "http://".$_SERVER['SERVER_NAME']."/index.php?id={{user.id}}";
                    $image_url = 'http://'.$_SERVER['SERVER_NAME'].'/images/Avatars/{{myUser.avatar}}';
                ?>
                <a ng-class="{'hidden': !simple}" ng-click="voteFB(myUser.id)" class='footer_fasebook' href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo  $url ; ?>&p[summary]=<?php echo $summary ?>&p[images][0]=<?php echo $image_url ?>" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" title="Поделиться ссылкой на Фейсбук" target="_parent"><img src='../images/popup_facebook.png' width='41' height='38'/></a>
                <?php
                    $description = 'Делитесь ссылкой на свои результаты "в контакте", и повышайте свой рейтинг';
                    $url = "http://".$_SERVER['SERVER_NAME']."/index.php?id={{myUser.id}}";
                    $image_url = 'http://'.$_SERVER['SERVER_NAME'].'/images/Avatars/{{myUser.avatar}}';
                ?>
                <a ng-class="{'hidden': !simple}" ng-click="voteVK(myUser.id)" class='footer_vk' href="http://vkontakte.ru/share.php?url=<?php echo $url; ?>&description=<?php echo $description; ?>&image=<?php echo $image_url; ?>&noparse=true" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" title="Сохранить в Вконтакте" target="_parent"><img src='../images/popup_vk.jpg' width='41' height='37'/></a>
                <a ng-class="{'hidden': simple}" class="footer_like_image" ng-click="vote(myUser.id)" class='footer_like' href=""><img src='../images/icons/like.png' width='33' height='27'/></a>
                <span class="footer_like_text">{{myUser.likes}}</span>
                <span style='position: absolute; right: 20px; top: 65px;'>{{voice_mess}}</span>
            </div>
        </div>
    </div>
    <div ng-click="closePopup()" class="popup_close"><img src="../images/popup_close.png" width="50" height="50"/></div>
</div>