<?php
$users      = new Users(); 
$user1_f    = $users->getUserData(1, 'popular');
$user2_f    = $users->getUserData(2, 'popular');
$user3_f    = $users->getUserData(3, 'popular');
$user1_p    = $users->getUserData(1, 'fast');
$user2_p    = $users->getUserData(2, 'fast');
$user3_p    = $users->getUserData(3, 'fast');
?>
<div class='slider_block'>
    <h1 class="block_name">результаты</h1> 

    <div class="slider_controlls">
        <a ng-click="switchPopular()" ng-class="{'dashed': !popularShow, 'results_active': popularShow}" id="showPopular" href="">САМЫЕ ПОПУЛЯРНЫЕ</a>
        <a ng-click="switchFast()" ng-class="{'dashed': popularShow, 'results_active': !popularShow}" id="showFast" href="">САМЫЕ БЫСТРЫЕ</a>
    </div>

    <a class="all_results" ng-click='seeAll()' href="">
        <img src="../images/main_trophy.png" width="50" height="50"/><br>
        <span>все</span><br><span>результаты</span>
    </a>

    <div id="viewPort">

        <div id="dinamicBlock">
            <div id="leftBlock">            
                <div class="places">
                    <ul>
                        <li ng-click='getUser(<?php echo $user2_p->id;?>)'>
                            <div class="place2">
                                <div class="place_number"><img src="images/Numbers/NB2.png" width="55" height="55"/></div>
                                <img src="data/Avatars/<?php echo $user2_p->avatar;?>" width="225" height="170"/>
                                <div class="place_name"><?php echo $user2_p->name1."<br>".$user2_p->name2;?></div>
                                <div class="place_time">
                                    <i class="fa fa-clock-o"></i>
                                    <span><?php echo $user2_p->time;?></span>
                                </div>
                            </div>
                        </li>
                        <li ng-click='getUser(<?php echo $user1_p->id;?>)'>
                            <div class="place1">
                                <div class="place_number"><img src="images/Numbers/NB1.png" width="55" height="55"/></div>
                                <img src="data/Avatars/<?php echo $user1_p->avatar;?>" width="225" height="170"/>
                                <div class="place_name"><?php echo $user1_p->name1."<br>".$user1_p->name2;?></div>
                                <div class="place_time">
                                    <i class="fa fa-clock-o"></i>
                                    <span><?php echo $user1_p->time;?></span>
                                </div>
                            </div>
                        </li>
                        <li ng-click='getUser(<?php echo $user3_p->id;?>)'>
                            <div class="place3">
                                <div class="place_number"><img src="images/Numbers/NB3.png" width="55" height="55"/></div>
                                <img src="data/Avatars/<?php echo $user3_p->avatar;?>" width="225" height="170"/>
                                <div class="place_name"><?php echo $user3_p->name1."<br>".$user3_p->name2;?></div>
                                <div class="place_time">
                                    <i class="fa fa-clock-o"></i>
                                    <span><?php echo $user3_p->time;?></span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="rightBlock">            
                <div class="places">
                    <ul>
                        <li ng-click='getUser(<?php echo $user2_f->id;?>)'>
                            <div class="place2">
                                <div class="place_number"><img src="images/Numbers/NB2.png" width="55" height="55"/></div>
                                <img src="data/Avatars/<?php echo $user2_f->avatar;?>" width="225" height="170"/>
                                <div class="place_name"><?php echo $user2_f->name1."<br>".$user2_f->name2;?></div>
                                <div class="place_time">
                                    <i class="fa fa-clock-o"></i>
                                    <span><?php echo $user2_f->time;?></span>
                                </div>
                            </div>
                        </li>
                        <li ng-click='getUser(<?php echo $user1_f->id;?>)'>
                            <div class="place1">
                                <div class="place_number"><img src="images/Numbers/NB1.png" width="55" height="55"/></div>
                                <img src="data/Avatars/<?php echo $user1_f->avatar;?>" width="225" height="170"/>
                                <div class="place_name"><?php echo $user1_f->name1."<br>".$user1_f->name2;?></div>
                                <div class="place_time">
                                    <i class="fa fa-clock-o"></i>
                                    <span><?php echo $user1_f->time;?></span>
                                </div>
                            </div>
                        </li>
                        <li ng-click='getUser(<?php echo $user3_f->id;?>)'>
                            <div class="place3">
                                <div class="place_number"><img src="images/Numbers/NB3.png" width="55" height="55"/></div>
                                <img src="data/Avatars/<?php echo $user3_f->avatar;?>" width="225" height="170"/>
                                <div class="place_name"><?php echo $user3_f->name1."<br>".$user3_f->name2;?></div>
                                <div class="place_time">
                                    <i class="fa fa-clock-o"></i>
                                    <span><?php echo $user3_f->time;?></span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>