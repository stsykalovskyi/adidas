<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
        <meta property="og:image" content="http://ezo-market.com.ua/images/logo_big.jpg"/>
        <meta property="og:title" content="Интернет-магазин EZO-market"/>
        <meta property="og:url" content="http://ezo-market.com.ua/"/>
        <meta property="og:description" content="Интернет-магазин EZO-market: Главная"/>
        <meta property="og:site_name" content="Интернет-магазин EZO-market"/>

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <link type="text/css"  rel="stylesheet" href="css/parts.css" />
        <link type="text/css"  rel="stylesheet" href="css/style.css" />
        
        <script type="text/javascript" src="js/jquery-latest.min.js"></script>
        <script type="text/javascript" src="js/angular.min.js"></script>
        <script type="text/javascript" src="js/jscript000.js"></script>
        <script type="text/javascript" src="js/json2.js"></script>
        <script type="text/javascript" src="js/storage.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body ng-app>    
<div  ng-controller="Results">
<div class="container" id="page">
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'adidas бобслей', 'url'=>array('/site/index'), 'itemOptions'=>array('class'=>'first_item')),
                                array('label'=>'О БОБСЛЕЕ', 'url'=>array('/site/page', 'view'=>'about')),
                                array('label'=>'РЕЗУЛЬТАТЫ', 'url'=>array('/site/page', 'view'=>'results')),
				array('label'=>'ПРАВИЛА УЧАСТИЯ', 'url'=>array('/site/page', 'view'=>'rules')),
				array('label'=>'КОЛЛЕКЦИЯ ЧЕМПИОНОВ', 'url'=>array(''), 'itemOptions'=>array('id'=>'new_blank')),
			),
		)); 
                $r = "";
                $view = '';
                if(isset($_GET['r']))$r      = $_GET['r'];
                if(isset($_GET['view']))$view   = $_GET['view'];
                ?>
		<ul id="yw0">
                    <li class="first_item<?php if($r=='site/index')echo " active";?>"><a href="/index.php?r=site/index">adidas бобслей</a></li>
                    <li<?php if($view=='about')echo " class='active'";?>><a href="/index.php?r=site/page&amp;view=about">О БОБСЛЕЕ</a></li>
                    <li<?php if($view=='results')echo " class='active'";?>><a href="/index.php?r=site/page&amp;view=results">РЕЗУЛЬТАТЫ</a></li>
                    <li<?php if($view=='rules')echo " class='active'";?>><a href="/index.php?r=site/page&amp;view=rules">ПРАВИЛА УЧАСТИЯ</a></li>
                    <li id="new_blank"><a target='_blank' href="http://www.adidas.ru/champions">КОЛЛЕКЦИЯ ЧЕМПИОНОВ</a></li>
                </ul>
                <a class="last_item" class='check_result' ng-click="showResult()" href=''>УЗНАТЬ СВОЙ РЕЗУЛЬТАТ<i class='fa fa-chevron-circle-right'></i></a>
	</div>
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
            <a href="http://www.adidas.ru/help-topics-terms_and_conditions.html">Юридическая информация</a>|<a href="http://www.adidas.ru/help-topics-privacypolicy.html">Обработка персональных данных</a>|<span>&copy; OOO "Адидас". Все права защищены.</span><br/>
	</div><!-- footer -->

</div><!-- page -->
<?php require_once 'protected/files/popup.php';?>
</div> 
    <script type="text/javascript" src="js/jscript001.js"></script>
</body>
</html>
