<?php
if(isset($_SESSION['id']))
{
    header('Location: index.php?r=site/page&view=results&id='.$_SESSION['id']);
}
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
$this->setPageTitle("Главная");
$users = new Users();
$user1 = $users->find('place=1');
$user2 = $users->find('place=2');
$user3 = $users->find('place=3');
?>
<div id="banner">
    <div class='main_big_text'>стань чемпионом этой зимы</div>
    <div ng-click="showResult()" id="banner_btn"></div>
</div>

<div id="how">
    <div class='how_big_text'>как<br>участвовать?</div>
    <div class='how_come_text'>Приди в европейский</div>
    <div class='how_registration_text'>Зарегистрируйся на заезд</div>
    <div class='how_meet_text'>Участвуй в заезде</div>
    <div class='how_champion_text'>Стань чемпионом</div>
    <div class='how_meet_image'>
        <img src='images/icons/bob.png' width='48' height='42'/>
    </div>
    <div class='how_registration_image'>
        <img src='images/icons/list.png' width='44' height='39'/>
    </div>
    <div class='how_champion_image'>
        <img src='images/icons/trophy.png' width='44' height='44'/>
    </div>
    <div class='how_come_image'>
        <img src='images/icons/Walk.png' width='24' height='43'/>
    </div>
</div>

<?php
require_once 'protected/files/slider.php';
?>

<div class="collection">
    <div class='block_name correction_0'>коллекция чемпионов</div>
    <a class="collection_link" target='_blank' href="http://www.adidas.ru/champions">СМОТРЕТЬ ВСЮ КОЛЛЕКЦИЮ</a>
    <div class="collection_show_block">
        <ul class="collection_show">
            <li>
                <a target='_blank' href='http://www.adidas.ru/kurtka-coach/G81814_110.html'>
                    <img src="images/position1.png" width="205" height="240"/>
                    <div class="collection_line"></div>
                    <span class="collection_position_name">Куртка Coach</span><br>
                    <span class="collection_sex">Мужчины</span>
                    <span class="collection_sport">Фитнес</span>
                </a>
            </li>
            <li>
                <a target='_blank' href='http://www.adidas.ru/tolstovka-crew/F47553_300.html'>
                    <img src="images/position2.png" width="205" height="240"/>
                    <div class="collection_line"></div>
                    <span class="collection_position_name">Толстовка Crew</span><br>
                    <span class="collection_sex">Мужчины</span>
                    <span class="collection_sport">Фитнес</span>
                </a>
            </li>
            <li>
                <a target='_blank' href='http://www.adidas.ru/zhil%D0%B5t-ut%D0%B5pl%D0%B5nnyi-primaloft/G81832_470.html'>
                    <img src="images/position3.png" width="205" height="240"/>
                    <div class="collection_line"></div>
                    <span class="collection_position_name">Жилет утепленный Primaloft</span><br>
                    <span class="collection_sex">Мужчины</span>
                    <span class="collection_sport">Фитнес</span>
                </a>
            </li>
            <li>
                <a target='_blank' href='http://www.adidas.ru/kurtka-university/G81816_110.html'>
                    <img src="images/position4.png" width="205" height="240"/>
                    <div class="collection_line"></div>
                    <span class="collection_position_name">Куртка University</span><br>
                    <span class="collection_sex">Мужчины</span>
                    <span class="collection_sport">Фитнес</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<?php
require_once 'protected/files/popup_data.php';
?>