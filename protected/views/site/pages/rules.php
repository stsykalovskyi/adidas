<?php
/* @var $this SiteController */

$this->setPageTitle("Правила участия");
?>

<div class="bob_rules_banner">

<div class="t1">
тебе, наверное, интересно, как выиграть<br />возможность прокатиться на настоящем<br />болиде по настоящей бобслейной<br />трассе?
</div>

</div>

<div class="bob_rules_game_map">
<div class="t1">как<br />участвовать?</div>    
<div class="t2">Приходи в тц Европейский<br />и докажи своё мастерство<br />на Бобслей-тренажере!</div>    
<div class="t3">Зарегистрируйся<br />на заезд</div>    
<div class="t4">Участвуй<br />в заезде</div>    
<div class="t5">Поделись своими результатами<br />в социальной сети</div>    
<div class="t6">Следи за ходом соревнования<br />на сайте winterwinner.ru</div>    
<div class="t7">Самые быстрые получат<br />шанс прокатиться<br />на настоящем бобслее!</div>  
<div class="my_result" ng-click="showResult()">узнать свой<br />результат</div>  
<img src="/images/kubok.png" ng-click="showResult()">
</div>


<div class="bob_rules_city_map">
<div class="bob_rules_contacts">
<div class="bob_rules_contacts_rel">

<div class="t1">где найти<br />
симулятор бобслея?</div>
<div class="t2">123022, г. Москва, ул. 2-я<br />Звенигородская, д. 13, стр. 41,<br />ст. метро “Киевская”</div>
<div class="t3">Телефон: +7 495 921-34-44</div>
<div class="t4">Время работы:</div>
<div class="t5">Подедельник, четверг, воскресенье<br />с 10:00 до 22:00</div>
<div class="t6">Пятница, суббота с 10:00 до 23:00</div>

</div>
</div>

<div class="bob_rules_white"></div>
<iframe src="https://mapsengine.google.com/map/embed?mid=zy0RK21g76j8.kVH4TgRHicF8&zoom=4" width="1265" height="500"></iframe>

</div>
