<?php
if(isset($_GET['filter']))
    $filter = $_GET['filter'];
if(empty($filter))
    $filter = 'popular';
$users      = new Users();
$user1      = $users->getUserData(1, $filter);
$user2      = $users->getUserData(2, $filter);
$user3      = $users->getUserData(3, $filter);
$users_7    = $users->getGroup(4, 7, $filter);
$users_20   = $users->getGroup(11, 20, $filter);
$dashed1    = '';
$dashed2    = '';
?>
<div>
<div class="results_banner">
    <div class="block_name correction_2">результаты</div>
    <div class='top3'>ТОП 3</div>
    
    <?php if($filter=='popular') echo "<div class='popular_block'></div>";?>
    <?php if($filter=='fast')$dashed1 = ' dashed';  echo '<a ng-click="switchPopular()" class="popular_link'.$dashed1.'" id="showPopular" href="index.php?r=site/page&view=results&filter=popular">САМЫЕ ПОПУЛЯРНЫЕ</a>';?>
    <?php if($filter=='fast') echo "<div class='fast_block'></div>";?>
    <?php if($filter=='popular')$dashed2 = ' dashed'; echo "<a ng-click='switchFast()' class='fast_link$dashed2' id='showFast' href='index.php?r=site/page&view=results&filter=fast'>САМЫЕ БЫСТРЫЕ</a>";?>
    
    
    <div class="my_position">
        <a ng-click="showResult()" class="my_position_link" href=""><i class="trophy"></i><span>моя</span><br><span>позиция</span></a>
    </div>
    
    <div class="places places_correction" ng-init="startResults()">
        <ul>
            <li ng-click="getUser(<?PHP echo $user2->id;?>)">
                <div class="place2">
                    <div class="place_number"><img src="images/Numbers/NB2.png" width="50" height="50"/></div>
                    <img src="images/Avatars/<?php echo $user2->avatar;?>" width="225" height="171"/>
                    <div class="place_name">
                        <span><?php echo $user2->name1;?></span><br>
                        <span><?php echo $user2->name2;?></span></div>
                    <div class="place_time">
                        <i class="fa fa-clock-o"></i>
                        <span><?php echo $user2->time;?></span>
                    </div>
                </div>
            </li>
            <li ng-click="getUser(<?PHP echo $user1->id;?>)">
                <div class="place1 correction_5">
                    <div class="place_number"><img src="images/Numbers/NB1.png" width="50" height="50"/></div>                    
                    <img src="images/Avatars/<?php echo $user1->avatar;?>" width="225" height="170"/>
                    <div class="place_name">
                        <span><?php echo $user1->name1;?></span><br>
                        <span><?php echo $user1->name2;?></span>
                    </div>
                    <div class="place_time">
                        <i class="fa fa-clock-o"></i>
                        <span><?php echo $user1->time;?></span>
                    </div>
                </div>
            </li>
            <li ng-click="getUser(<?PHP echo $user3->id;?>)">
                <div class="place3 correction_4">
                    <div class="place_number"><img src="images/Numbers/NB3.png" width="50" height="50"/></div>
                    <img ng-src="images/Avatars/<?php echo $user3->avatar;?>" width="225" height="170"/>
                    <div class="place_name">
                        <span><?php echo $user3->name1?></span><br>
                        <span><?php echo $user3->name2;?></span></div>
                    <div class="place_time">
                        <i class="fa fa-clock-o"></i>
                        <span><?php echo $user3->time;?></span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="results_10">
    <ul class="results_10_list" ng-init="getSeven()">
        <?php foreach ($users_7 as $user){?>
        <li style='cursor: pointer;' ng-click="getUser(<?php echo $user->id;?>)">
            <div class='results_7_number'><img ng-src="images/Numbers/NS<?php echo $user->place;?>.png" width="30" height="30"/></div>
            <img ng-src="images/Avatars/<?php echo $user->avatar;?>" width="122" height="92"/>
            <div class='results_7_text'>
                <span><?php echo $user->name1;?></span><br>
                <span><?php echo $user->name2;?></span></div>
            <div class='results_7_time'><i class='fa fa-clock-o'></i><?php echo $user->time;?></div>
        </li>
        <?php }?>
    </ul>
</div>
<div class='results_table'>
    <table>
        <thead ng-click="checkSimple()">
        <th>место</th>
        <th>как зовут</th>
        <th>виражей</th>
        <th>скорость</th>
        <th>время</th>
        <th>ID</th>
        <td>likes</td>
        </thead>
        <tbody>
            <?php foreach($users_20 as $user){?>
            <tr ng-click="getUser(<?PHP echo $user->id;?>)">
                <td><?php echo $user->place;?></td>
                <td><?php echo $user->name1." ".$user->name2;?></td>
                <td><?php echo $user->curves;?></td>
                <td><?php echo $user->speed;?> км/ч</td>
                <td><?php echo $user->time;?></td>
                <td><?php echo $user->id;?></td>
                <td><?php echo $user->likes;?></td>
            </tr>
            <?php }?>
            <tr ng-class="user.id" ng-repeat='user in users' ng-click='getUser(user.id)'>
                <td>{{user.place}}</td>
                <td>{{user.name1}} {{user.name2}}</td>
                <td>{{user.curves}}</td>
                <td>{{user.speed}} км/ч</td>
                <td>{{user.time}}</td>
                <td>{{user.id}}</td>
                <td>{{user.likes}}</td>
            </tr>
        </tbody>
    </table>
</div>
<a ng-click="showMoreResults()" class='refresh' href=''><i class='icon_refresh'></i><div class='refresh_text'>показать еще</div></a>
</div>
<?php 
require_once 'protected/files/popup_data.php';
require_once 'protected/files/popup_single.php';
?>