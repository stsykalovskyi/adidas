<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property integer $place
 * @property string $name
 * @property integer $curves
 * @property double $speed
 * @property string $time
 * @property string $email
 * @property string $avatar
 * @property string $phone
 * @property string $surname
 */
class User
{
    public $name1;
    public $name2;
    public $avatar;
    public $speed;
    public $time;
    public $curves;
    public $place;
    public $id;
}
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('place, curves', 'numerical', 'integerOnly'=>true),
			array('speed', 'numerical'),
			array('name, avatar', 'length', 'max'=>50),
			array('email, phone, surname', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, place, name, curves, speed, time, email, avatar, phone, surname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'place' => 'Place',
			'name' => 'Имя',
			'surname' => 'Surname',
			'curves' => 'Виражей',
			'speed' => 'Скорость',
			'time' => 'Время',
			'email' => 'Email',
			'phone' => 'Телефон',
			'avatar' => 'Аватар',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('place',$this->place);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('curves',$this->curves);
		$criteria->compare('speed',$this->speed);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('avatar',$this->avatar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getUserById($id, $filter)
        {
                $sql        = " SELECT * 
                                FROM `users` 
                                WHERE `id` = $id;";
                                
                $users          = new Users();
                $result         = $users->findAllBySql($sql);
                $place          = $this->getPlace($id, $filter);
                $user           = $this->getUserData($place, $filter);
                
                return $user;
        }
        
        public function getPlace($id, $filter){
            
                if($filter=='popular'){
                    $sql        = " SELECT * 
                                    FROM `users` 
                                    ORDER BY `likes` DESC, `id` ASC;";
                }else{
                    $sql        = " SELECT * 
                                    FROM `users` 
                                    ORDER BY `time` ASC, `id` ASC;";
                }
                
                $users      = new Users();
                $result     = $users->findAllBySql($sql);
                $user_      = new User();
                
                $place = 1;
                foreach ($result as $user){
                {
                     if($id == $user->id){
                         return $place;
                     }
                     $place++;
                }
            }
        }


        public function getUserData($place, $filter)
        {
            $number = $place-1;
            if($filter!='popular'){
                $sql        = " SELECT * 
                                FROM `users` 
                                ORDER BY `time`, `id` ASC
                                LIMIT $number,1;";}
                else{
                    $sql    = " SELECT *
                                FROM `users`
                                ORDER BY `likes` DESC, `id` ASC
                                LIMIT $number,1;";
                }
                $users          = new Users();
                $request        = array();
                $filtered       = $users->findAllBySql($sql);
                $user_ = new User();
                foreach ($filtered as $user){
                    $user_->place             = $place;
                    $fixed_str                = preg_replace('/[\s]{2,}/', ' ', $user->name);
                    $fixed_str                = trim($fixed_str);
                    $name_array               = explode(" ", $fixed_str);
                    $user_->name1             = $name_array[0];
                 if(isset($name_array[1]))
                    $user_->name2             = $name_array[1];
                    $user_->time              = $user->time;
                    $user_->avatar            = $user->avatar;
                    $user_->id                = $user->id; 
                    $user_->likes             = $user->likes;  
                    $user_->speed             = $user->speed;
                    $user_->curves            = $user->curves;  
                }
                    if($user_->name1==NULL)$user_->name1='empty';
                    if($user_->name2==NULL)$user_->name2='';
                    if($user_->time==NULL)$user_->time='00:00:00';
                    if($user_->avatar==NULL)$user_->avatar='empty.png';
                return $user_;
        }
        
        public function getGroup($first, $count, $filter){
            $first--;
            if($filter!='popular'){
                $sql        = " SELECT * 
                                FROM `users` 
                                ORDER BY `time` ASC, `id` ASC
                                LIMIT $first,$count;";}
                else{
                    $sql    = " SELECT *
                                FROM `users`
                                ORDER BY `likes` DESC, `id` ASC
                                LIMIT $first,$count;";
                }
                
                $users          = new Users();
                $request        = array();
                $filtered       = $users->findAllBySql($sql);
                $user_ = new User();
                $place = $first+1;
                foreach ($filtered as $user){
                    $user_ = new User();
                    $user_->place             = $place;
                    $fixed_str                = preg_replace('/[\s]{2,}/', ' ', $user->name);
                    $fixed_str                = trim($fixed_str);
                    $name_array               = explode(" ", $fixed_str);
                    $user_->name1             = $name_array[0];
                 if(isset($name_array[1]))
                    $user_->name2             = $name_array[1];
                    $user_->time              = $user->time;
                    $user_->speed             = $user->speed;
                    $user_->curves            = $user->curves;
                    $user_->id                = $user->id;  
                    $user_->avatar            = $user->avatar;  
                    $user_->likes             = $user->likes;  
                    array_push($request, $user_);
                    $place++;
                }
                return $request;
        }
        public function voteComputing($id){
            $ip     = $_SERVER['REMOTE_ADDR'];
            echo $ip; exit;
            $users  = new Users();
            $ips    = $users->find("`ip`='$ip'");
            $find   = false;
            foreach ($ips as $ip)
            {
                //if($ip->)
            }
        }
}
