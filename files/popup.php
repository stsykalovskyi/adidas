<div ng-class="{'overlay': showPopup}" ng-click='showPopup=false'></div>
<div ng-class="{'hidden': !showPopup, 'popup': showPopup}">
    <div class="popup_body">
        <div ng-show="!userDefined" class='padding_20'>
            <div style='text-align: center; font-size: 22px !important;'>Для того, чтобы узнать результат нужно отправить свой телефон или Email</div><br><br>
            <form ng-submit="findUser()" name="findForm">
                    <input style='text-align: center; background: #F0F0F0;' class="popup_input" ng-model="user.requisites" name="email" type="test" placeholder='Email или телефон'/><br>
                    <span class="error">{{message}}</span>
                    <br><div class='popup_button' ng-click='findUser()'></div>
            </form>
        </div>
        <div ng-show="userDefined">
            <div class='popup_line'>
                <img ng-src="{{user.avatar}}" width="150" height="150"/>
                <span class='popup_text1'>Мой результат заезда</span>
                <div class='popup_place'>{{user.place}}</div>
                <img class='popup_time_image' class='popup_image' src='../images/popup_time.png' width='50' height='50'/>
                <span class='popup_time'>{{user.time}}</span>
                <span class='popup_text2'>Ваше время</span>
            </div>
            <div class='popup_line'>
                <img class='curves_image' src='../images/popup_curves.jpg' width='30' height='30'/>
                <span class='curves_count'>{{user.curves}}</span>
                <span class='curves_text'>вираж</span>
                <img class='speed_image' src='../images/popup_speed.jpg' width='43' height='37'/>
                <span class='speed_count'>{{user.speed}} км/ч</span>
                <span class='speed_text'>средняя скорость</span>
            </div>            
            <div class='popup_footer'>
                <img class='popup_to_tell' src='../images/popup_to_tell.png' width='207' height='45'/>
                <a class='footer_fasebook' href='#'><img src='../images/popup_facebook.png' width='41' height='38'/></a>
                <a class='footer_vk' href='#'><img src='../images/popup_vk.jpg' width='41' height='37'/></a>
            </div>
        </div>
    </div>
    <div ng-click="closePopup()" class="popup_close"><img src="../images/popup_close.png" width="30" height="30"/></div>
</div>