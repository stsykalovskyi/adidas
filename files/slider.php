
<div id="viewPort">
    
            <h1 class="block_name">результаты</h1> 
            <div class="slider_controlls">
                <a ng-click="switchPopular()" ng-class="{'dashed': !popularShow, 'results_active': popularShow}" id="showPopular" href="">САМЫЕ ПОПУЛЯРНЫЕ</a>
                <a ng-click="switchFast()" ng-class="{'dashed': popularShow, 'results_active': !popularShow}" id="showFast" href="">САМЫЕ БЫСТРЫЕ</a>
            </div>
            
            <a class="all_results" ng-click='seeAll()' href="">
                <img src="../images/main_trophy.png" width="50" height="50"/><br>
                <span>все</span><br><span>результаты</span>
            </a>
    
    <div id="dinamicBlock">
        <div id="leftBlock">            
            <div class="places">
                <ul>
                    <li>
                        <div class="place2">
                            <div class="place_number"><img src="images/Numbers/NB2.png" width="55" height="55"/></div>
                            <img src="<?php echo $user2->avatar;?>" width="225" height="170"/>
                            <div class="place_name"><?php echo $user2->name."<br>".$user2->surname;?></div>
                            <div class="place_time">
                                <i class="fa fa-clock-o"></i>
                                <span><?php echo $user2->time;?></span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="place1">
                            <div class="place_number"><img src="images/Numbers/NB1.png" width="55" height="55"/></div>
                            <img src="<?php echo $user1->avatar;?>" width="225" height="170"/>
                            <div class="place_name"><?php echo $user1->name."<br>".$user1->surname;?></div>
                            <div class="place_time">
                                <i class="fa fa-clock-o"></i>
                                <span><?php echo $user1->time;?></span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="place3">
                            <div class="place_number"><img src="images/Numbers/NB3.png" width="55" height="55"/></div>
                            <img src="<?php echo $user3->avatar;?>" width="225" height="170"/>
                            <div class="place_name"><?php echo $user3->name."<br>".$user3->surname;?></div>
                            <div class="place_time">
                                <i class="fa fa-clock-o"></i>
                                <span><?php echo $user3->time;?></span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div id="rightBlock">            
            <div class="places">
                <ul>
                    <li>
                        <div class="place2">
                            <div class="place_number"><img src="images/Numbers/NB2.png" width="55" height="55"/></div>
                            <img src="<?php echo $user2->avatar;?>" width="225" height="170"/>
                            <div class="place_name"><?php echo $user2->name."<br>".$user2->surname;?></div>
                            <div class="place_time">
                                <i class="fa fa-clock-o"></i>
                                <span><?php echo $user2->time;?></span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="place1">
                            <div class="place_number"><img src="images/Numbers/NB1.png" width="55" height="55"/></div>
                            <img src="<?php echo $user1->avatar;?>" width="225" height="170"/>
                            <div class="place_name"><?php echo $user1->name."<br>".$user1->surname;?></div>
                            <div class="place_time">
                                <i class="fa fa-clock-o"></i>
                                <span><?php echo $user1->time;?></span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="place3">
                            <div class="place_number"><img src="images/Numbers/NB3.png" width="55" height="55"/></div>
                            <img src="<?php echo $user3->avatar;?>" width="225" height="170"/>
                            <div class="place_name"><?php echo $user3->name."<br>".$user3->surname;?></div>
                            <div class="place_time">
                                <i class="fa fa-clock-o"></i>
                                <span><?php echo $user3->time;?></span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>