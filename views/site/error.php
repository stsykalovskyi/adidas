<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = "Ошибка";
?>


<div style='color: white;'>
    <div class='block_bg'>
        <span class='block_name'>Ошибка <?php echo $code; ?></span>
        <div style='position: absolute; text-align: center; font-size: 25px; left: 50px; top: 200px;'>
            <?php echo CHtml::encode($message); ?>
        </div>
    </div>
</div>