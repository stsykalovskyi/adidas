<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
$this->setPageTitle("Главная");
$users = new Users();
$user1 = $users->find('place=1');
$user2 = $users->find('place=2');
$user3 = $users->find('place=3');
?>
<div id="banner">
    <div ng-click="showResult()" id="banner_btn"></div>
</div>

<div id="how"></div>

<?php
require_once 'protected/files/slider.php';
?>

<div ng-controller="Collection" class="collection">
    <div class='block_name correction_0'>коллекция чемпионов</div>
    <a class="collection_link" href="http://www.adidas.ru/champions">СМОТРЕТЬ ВСЮ КОЛЛЕКЦИЮ</a>
    <div class="collection_show_block">
        <ul class="collection_show">
            <li>
                <img src="images/position1.png" width="205" height="240"/>
                <div class="collection_line"></div>
                <span class="collection_position_name">Куртка Coach</span><br>
                <span class="collection_sex">Мужчины</span>
                <span class="collection_sport">Фитнес</span>
            </li>
            <li>
                <img src="images/position2.png" width="205" height="240"/>
                <div class="collection_line"></div>
                <span class="collection_position_name">Толстовка Crew</span><br>
                <span class="collection_sex">Мужчины</span>
                <span class="collection_sport">Фитнес</span>
            </li>
            <li>
                <img src="images/position3.png" width="205" height="240"/>
                <div class="collection_line"></div>
                <span class="collection_position_name">Жилет утепленный Primaloft</span><br>
                <span class="collection_sex">Мужчины</span>
                <span class="collection_sport">Фитнес</span>
            </li>
            <li>
                <img src="images/position4.png" width="205" height="240"/>
                <div class="collection_line"></div>
                <span class="collection_position_name">Куртка University</span><br>
                <span class="collection_sex">Мужчины</span>
                <span class="collection_sport">Фитнес</span>
            </li>
        </ul>
    </div>
</div>
<div ng-controller="Twitter" class="twitter_bg">
    <div class="block_name correction_1">новости олимпиады</div>
    <a class="twitter_link" href="https://twitter.com/sochi2014_ru">ОФИЦИАЛЬНЫЙ АККАУНТ ОЛИМПИАДЫ</a>
    <div class="twitter_block">
        <ul>
            <li>
                <div class="twitter_text">
                    <span>Готовим вместе санки на старт - командная эстафета впервые в #Сочи.</span>
                </div>
                <img class="twitter_frame" src="../images/twitter_frame.png" width="161"/>
                <span class="twitter_text_blue">@sochi2014_ru</span><br>
                <div class="twitter_bottom">
                    <img src="../images/twitter_pic1.png"/><span>2 hours ago</span>
                </div>
            </li>
            <li>
                <div class="twitter_text">
                    <span>Готовим вместе санки на старт - командная эстафета впервые в #Сочи.</span>
                </div>
                <img class="twitter_frame" src="../images/twitter_frame.png" width="161"/>
                <span class="twitter_text_blue">@sochi2014_ru</span><br>
                <div class="twitter_bottom">
                    <img src="../images/twitter_pic1.png"/><span>2 hours ago</span>
                </div>
            </li>
            <li>
                <div class="twitter_text">
                    <span>Готовим вместе санки на старт - командная эстафета впервые в #Сочи.</span>
                </div>
                <img class="twitter_frame" src="../images/twitter_frame.png" width="161"/>
                <span class="twitter_text_blue">@sochi2014_ru</span><br>
                <div class="twitter_bottom">
                    <img src="../images/twitter_pic1.png"/><span>2 hours ago</span>
                </div>
            </li>
            <li>
                <div class="twitter_text">
                    <span>Готовим вместе санки на старт - командная эстафета впервые в #Сочи.</span>
                </div>
                <img class="twitter_frame" src="../images/twitter_frame.png" width="161"/>
                <span class="twitter_text_blue">@sochi2014_ru</span><br>
                <div class="twitter_bottom">
                    <img src="../images/twitter_pic1.png"/><span>2 hours ago</span>
                </div>
            </li>
        </ul>
    </div>
</div>