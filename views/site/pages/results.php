<?php
    $this->setPageTitle("Результаты");
    $users = new Users();
    $user1 = $users->find('place=1');
    $user1_name = explode(" ", $user1->name);
    $user2 = $users->find('place=2');
    $user2_name = explode(" ", $user2->name);
    $user3 = $users->find('place=3');
    $user3_name = explode(" ", $user3->name);
    $users_10 = $users->findAllBySql("SELECT * FROM `users` WHERE `place`>3 AND `place`< 11;");
    if(isset($_GET['filter']))$filter = $_GET['filter'];
    if(empty($filter))
        $filter = 'fast';
?>
<div>
<div class="results_banner">
    <div class="block_name correction_2">результаты</div>
    <div class='top3'>ТОП 3</div>
    
    <div class='popular_link'>
        <a ng-click="switchPopular()" ng-class="{'dashed': !popularShow, 'bordered': popularShow}" id="showPopular" href="">САМЫЕ ПОПУЛЯРНЫЕ</a>
    </div>
    <div class='fast_link'>
        <a ng-click="switchFast()" ng-class="{'dashed': popularShow, 'bordered': !popularShow}" id="showFast" href="">САМЫЕ БЫСТРЫЕ</a>
    </div>
    <div class="my_position">
        <a ng-click="showResult()" class="my_position_link" href=""><i class="trophy"></i><span>моя</span><br><span>позиция</span></a>
    </div>
    
    <div class="places places_correction">
        <ul>
            <li>
                <div class="place2">
                    <div class="place_number"><img src="images/Numbers/NB2.png" width="50" height="50"/></div>
                    <img src="<?php echo $user2->avatar;?>" width="225" height="171"/>
                    <div class="place_name"><?php echo $user2->name."<br>".$user2->surname;?></div>
                    <div class="place_time">
                        <i class="fa fa-clock-o"></i>
                        <span><?php echo $user2->time;?></span>
                    </div>
                </div>
            </li>
            <li>
                <div class="place1 correction_5">
                    <div class="place_number"><img src="images/Numbers/NB1.png" width="50" height="50"/></div>                    
                    <img src="<?php echo $user1->avatar;?>" width="225" height="170"/>
                    <div class="place_name"><?php echo $user1->name."<br>".$user1->surname;?></div>
                    <div class="place_time">
                        <i class="fa fa-clock-o"></i>
                        <span><?php echo $user1->time;?></span>
                    </div>
                </div>
            </li>
            <li>
                <div class="place3 correction_4">
                    <div class="place_number"><img src="images/Numbers/NB3.png" width="50" height="50"/></div>
                    <img src="<?php echo $user3->avatar;?>" width="225" height="170"/>
                    <div class="place_name"><?php echo $user3->name."<br>".$user3->surname;?></div>
                    <div class="place_time">
                        <i class="fa fa-clock-o"></i>
                        <span><?php echo $user3->time;?></span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="results_10">
    <ul class="results_10_list">
        <?php  $i = 4; foreach ($users_10 as $user){?>
        <li>
            <div class='results_7_number'><img src="images/Numbers/NS<?php echo $i;?>.png" width="30" height="30"/></div>
            <img src="<?php echo $user->avatar;?>" width="122" height="92"/>
            <div class='results_7_text'><?php echo $user->name;?><br><?php echo $user->surname;?></div>
            <div class='results_7_time'><i class='fa fa-clock-o'></i><?php echo $user->time?></div>
        </li>
        <?php $i++;}?>
    </ul>
</div>
<div class='results_table'>
    <table>
        <thead>
        <th>место</th>
        <th>как зовут</th>
        <th>виражей</th>
        <th>скорость</th>
        <th>время</th>
        </thead>
        <tbody>
            <tr ng-click='showInfo(user.place)' ng-repeat="user in users">
                <td>{{user.place}}</td>
                <td>{{user.name}} {{user.surname}}</td>
                <td>{{user.curves}}</td>
                <td>{{user.speed}} км/ч</td>
                <td>{{user.time}}</td>
            </tr>
        </tbody>
    </table>
</div>
<a ng-click="showMoreResults()" class='refresh' href=''><i class='icon_refresh'></i><div class='refresh_text'>показать еще</div></a>
</div>